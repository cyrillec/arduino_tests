;hello.asm
;  turns on an LED which is connected to PB5 (digital out 13)

.include "./m328Pdef.inc"

Start:
	ldi r16,0b00100000
	out DDRB,r16
Loop:
	ldi r16,0b00100000
	out PortB,r16
        rcall Delay
	ldi r16,0b00000000
	out PortB,r16
        rcall Delay
	rjmp Loop


Infinity:
	rjmp Infinity

        
Delay:
    push r18
    push r19
    push r20
    ldi  r18, 102
    ldi  r19, 118
    ldi  r20, 194
L1: dec  r20
    brne L1
    dec  r19
    brne L1
    dec  r18
    brne L1
    pop r20
    pop r19
    pop r18
    ret

