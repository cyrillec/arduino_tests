#define F_CPU 16000000UL

#define BAUD  9600

#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

// fcpu/16  ASYNC_Normal
#define BAUD_PRESCALE(fcpu,br) ((fcpu / 16 / br) - 1)
// fcpu/2  SYNC_Normal
//#define BAUD_PRESCALE(fcpu,br) ((fcpu / 2 / br) - 1)

void serial_send_char(char c, FILE *stream){
  while (!( UCSR0A & (1<<UDRE0))); //TX available
  UDR0 = c;
}

char serial_get_char(FILE *stream){
   while (!( UCSR0A & (1<<RXC0)));
   return UDR0;
}

FILE uart_output = FDEV_SETUP_STREAM(serial_send_char, NULL, _FDEV_SETUP_WRITE);
FILE uart_input = FDEV_SETUP_STREAM(NULL, serial_get_char, _FDEV_SETUP_READ);

void serial_init(unsigned int baud){
        unsigned int ubrr = BAUD_PRESCALE(F_CPU, BAUD) ;

        UBRR0H = (ubrr>>8);
        UBRR0L = (ubrr & 0xff);

        // Enable TX
        UCSR0B |= (1<<TXEN0);
        UCSR0B |= (1<<RXEN0);

        // ASYNC_Normal
        UCSR0C &= ~(1<<UMSEL00);
        UCSR0C &= ~(1<<UMSEL01);
        // Parity disabled
        UCSR0C &= ~(1<<UPM00);
        UCSR0C &= ~(1<<UPM01);
        // 1 Stop
        UCSR0C &= ~(1<<USBS0);
        // 8 bits
        UCSR0C |= (1<<UCSZ00);
        UCSR0C |= (1<<UCSZ01);
}

unsigned char data_bss[]="This is BSS\n";

int main()
{
	int i = 0;

        unsigned char *data_malloc = malloc(10);
        unsigned char data_stack[256];

        serial_init(BAUD);
        stdout = &uart_output;
        stdin  = &uart_input;

        fscanf(stdin, "%s",data_stack);
        printf("Hello %s \n", data_stack);

	while(1){
          //serial_send_msg(data);
          printf("Printing addresses:\n");
          printf("- BSS: %p \n", data_bss);
          printf("- HEAP:%p \n", data_malloc);
          printf("- STACK: %p \n", data_stack);
          _delay_ms(1000);
       }
}
